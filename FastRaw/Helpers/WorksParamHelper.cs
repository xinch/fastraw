﻿using System.ComponentModel;

namespace FastRaw.Helpers
{
    class WorksParamHelper:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _txtMlvPath;

        private string _txtUnpackPath;

        private void PropChanged(string propName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        public string TxtMlvPath
        {
            get { return _txtMlvPath; }
            set
            {
                if (value != _txtMlvPath)
                {
                    _txtMlvPath = value;
                    PropChanged("TxtMlvPath");
                }
            }
        }

        public string TxtUnpackPath
        {
            get { return _txtUnpackPath; }
            set
            {
                if (value != _txtUnpackPath)
                {
                    _txtUnpackPath = value;
                    PropChanged("TxtUnpackPath");
                }
            }
        }
    }
}
