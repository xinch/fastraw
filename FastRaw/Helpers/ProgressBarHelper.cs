﻿using System.ComponentModel;

namespace FastRaw.Helpers
{
    class ProgressBarHelper:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int _value;

        private int _max;

        public ProgressBarHelper()
        {
            _max = 1;
        }

        private void PropChanged(string propName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        public int Value
        {
            get { return _value; }
            set
            {
                if (value != _value)
                {
                    _value = value;
                    PropChanged("Value");
                }
            }
        }

        public int Max
        {
            get { return _max; }
            set
            {
                if (value != _max)
                {
                    _max = value;
                    PropChanged("Max");
                }
            }
        }
    }
}
