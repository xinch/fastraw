﻿using System.ComponentModel;

namespace FastRaw.Helpers
{
    /// <summary>
    /// класс содержит enum  и наследует propertychanged дабы ловит изменения выбора радиобаттонах. 
    /// </summary>
    class EnumRadioButtonHelper: INotifyPropertyChanged
    {
        public enum WorksParams
        {
            CopyUnpack,
            CopyOnly,
            UnpackOnly
        }

        private WorksParams _selectedWorksParams;

        public WorksParams SelectedWorksParams
        {
            set
            {
                _selectedWorksParams = value;
                _PropertyChanged("SelectedWorksParams");
            }

            get { return _selectedWorksParams; }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void _PropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
        #endregion
    }
}
