﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using FastRaw.Helpers;
using Microsoft.VisualBasic.FileIO;
using MessageBox = System.Windows.MessageBox;

namespace FastRaw
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly TextBoxesHelper _textPanel;
        private readonly EnumRadioButtonHelper _enumRadioButtonHelper;
/*
        private ProgressBarHelper _barHelper;
*/

        public MainWindow()
        {
            InitializeComponent();

            #region загоняем dataContext

            _textPanel = new TextBoxesHelper();
             _enumRadioButtonHelper = new EnumRadioButtonHelper();
           // _barHelper = new ProgressBarHelper();

            if (TextPanel != null)
            {
                TextPanel.DataContext = _textPanel;
            }

            if (RadioButtonsPanel != null)
            {
                RadioButtonsPanel.DataContext = _enumRadioButtonHelper;
            }

            //if (CopyProgressBar != null)
            //{             
            //    CopyProgressBar.DataContext = _barHelper;
            //}

            #endregion
        }

        /// <summary>
        /// вызывает диалог выбора папки
        /// </summary>
        /// <returns> вызващает указатель на объект диалога</returns>
        private new static FolderBrowserDialog ShowDialog()
        {
            var dialog = new FolderBrowserDialog();

            dialog.ShowDialog();
            return dialog;
        }

        #region забиваем пути в текстбоксы из диалога

        private void BtnMlbPath_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = ShowDialog();
            _textPanel.TxtMlvPath = dialog.SelectedPath;
        }

        private void BtnUnpackPath_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = ShowDialog();
            _textPanel.TxtUnpackPath = dialog.SelectedPath;
        }

        #endregion

        private void BtnRed_onClick(object sender, RoutedEventArgs e)
        {
            // Проверка путей
            if (Directory.Exists(_textPanel.TxtUnpackPath) && Directory.Exists(_textPanel.TxtMlvPath))
            {
                CopyFromCard();
            }
            else
            {
                MessageBox.Show("Проверте пути, возможно они пустые");
            }           
        }
        
        //todo Прогресс бар

        private void CopyFromCard()
        {
            // Список всех файлов c выборкой по нужным расширениям
            var files = Directory.GetFiles(_textPanel.TxtMlvPath, "*.*").
                Where(str => str.ToLower().EndsWith(".raw") || str.ToLower().EndsWith(".mlv"));

            var lstFiles = files as IList<string> ?? files.ToList();

            // Проверка на существование директории и её создание
            if (!Directory.Exists(_textPanel.TxtUnpackPath))
            {
                Directory.CreateDirectory(_textPanel.TxtUnpackPath);
            }

            // Проверка, есть ли нужные файлы
            if (lstFiles.Any())
            {
                //_barHelper.Max = lstFiles.Count()-1;
                
                // выбираем порядок работы на основе выбора radioButtons
                switch (_enumRadioButtonHelper.SelectedWorksParams)
                {
                    case EnumRadioButtonHelper.WorksParams.CopyUnpack:
                    {  
                        DoWork(lstFiles,true);                       
                        break;
                    }
                    case EnumRadioButtonHelper.WorksParams.CopyOnly:
                    {
                        DoWork(lstFiles,false);
                        break;
                    }
                    case EnumRadioButtonHelper.WorksParams.UnpackOnly:
                    {
                        //TODO
                        break;
                    }
                    default:
                    {
                        MessageBox.Show("Select works params");
                        break;
                    }
                }
            }
        }

        private void DoWork(IList<string> lstFiles, bool ifunpack)
        {
            //работаем в потоке
            var copyThread = new Thread(() =>
            {
                for (int i = 0; i < lstFiles.Count(); i++)
                {
                    // Remove path from the file name.
                    string curFile = lstFiles[i].Substring(_textPanel.TxtMlvPath.Length + 1);

                    // Создаем новую папку и даем ей номер
                    //string incdir = waytocopy + @"\" + k + @"\";
                    var newFolder = _textPanel.TxtUnpackPath + @"\" + curFile + @"\";

                    Directory.CreateDirectory(newFolder);

                    FileSystem.CopyFile(lstFiles[i], newFolder + @"\" + curFile, UIOption.AllDialogs);
                    //File.Copy(lstFiles[i], newFolder + @"\" + curFile, true);
                    Dispatcher.BeginInvoke(new Action(() => BtnRed.IsEnabled = false));

                    //todo сделать очередь, дабы не запускать сразу много распаковок
                    if (ifunpack)
                    {
                        StartMlv(newFolder, curFile);
                    }
                }
                Dispatcher.BeginInvoke(new Action(() => BtnRed.IsEnabled = true));
                MessageBox.Show("Job is done");
            }) {IsBackground = true};
            copyThread.Start();
        }

        /// <summary>
        ///  запускает mlv дампер
        /// </summary>
        /// <param name="path">путь к новой папке, куда будем распаковывать</param>
        /// <param name="curFile">путь к текущему файлу</param>
        private void StartMlv(string path, string curFile)
        {
            // Приложение, которое будем запускать. Файл должен лежать рядом с EasyRaw.exe
            var mlvPath = new StringBuilder();
            mlvPath.Append(Directory.GetCurrentDirectory()).Append(ConsoleParams.ProgramName);

            // задание параметров запуска.
            // TODO Проблема с пониманием русских символов
            var mlvParams = new StringBuilder();
            var curFileDelExet = curFile.Remove(curFile.LastIndexOf(".", StringComparison.Ordinal));

            mlvParams.Append(Gettype(curFile)).Append('"').Append(path).Append(curFileDelExet).Append("_").Append('"').Append(" ").Append('"').Append(path).Append(curFile).Append('"');
            using (var mlvProcess = new Process())
            {
                mlvProcess.StartInfo.FileName = mlvPath.ToString();
                mlvProcess.StartInfo.Arguments = mlvParams.ToString();
                mlvProcess.Start();
                // если раскоментить, то пока не распакуеться, копирование не продолжиться                 
                //mlvProcess.WaitForExit();
            }
        }

        /// <summary>
        /// выдергивает тип исходника по расширению файла
        /// </summary>
        /// <param name="s">путь к файлу</param>
        /// <returns> возвращает параметра запуска для определенного типа исходника</returns>
        private string Gettype(string s)
        {
            s = s.ToLower();

            if (s.Contains("mlv"))
            {
                return ConsoleParams.DefaultParamsMlv;
            }
            if (s.Contains("raw"))
            {
                return ConsoleParams.DefaultParamsRaw;
            }
            return "";
        }
    }
}
